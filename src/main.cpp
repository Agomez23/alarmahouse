#include <Arduino.h> //
#include <FirebaseESP32.h> //Respectivas librerias de arduino y del ESP32 para usar con la base de datos de firebase
#include <WiFi.h> //

#define WIFI_SSID "agusRed" // wifi nombre
#define WIFI_PASSWORD "martillo" //wifi contraseña     
//no nos fue posible hacer que el ESP32 se conecte a cualquier red wifi que tenga a su alcance
#define sensorPin 22         //pin que va conectado al sensor
#define alarmaPin 23       //pin que va conectado al buzzer y a los leds
#define FIREBASE_HOST "tpmoroni-ab269-default-rtdb.firebaseio.com" //host de firebase
#define FIREBASE_AUTH "rpvXcFbmpT4ME3AtGvs77B8uzbun4PZj0Vyz1Qgz" // autenticador de firebase
FirebaseData firebaseData;

void setup () 
{
  pinMode(sensorPin, INPUT); //declaracion de pines, este es input ya que leera lo que trae el sensor
  pinMode(alarmaPin, OUTPUT); //y este de salida porque es el que hara funcionar una vez se active la alarma en si
  
  Serial.begin(9600); //empieza la conexion en serie
  // conectamos a wifi
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  Serial.print("conectando");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }
  Serial.println();
  Serial.print("conectado: ") ;
  Serial.println(WiFi.localIP());
  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);
  Firebase.reconnectWiFi(true);     
} //este while se ejecuta hasta que el ESP32 se conecte a la red wifi que queremos
void loop (){ //una vez conectado a wifi empieza el funcionamiento de pedido y apagado de la alarma

  if(Firebase.getString(firebaseData, "/Prenderalarma")) //este if funciona para verificar le funcionamiento correcto de firebase, ya que en el caso que no se obtenga ningun dato de prender alarma saltara el else que basicamente printea error en el codigo y la razon de firebase del error
  {
    String estadoalarma = firebaseData.stringData(); //la string estado alarma recoge el dato de firebase y lo guarda
    if(estadoalarma.toInt() == 1){  //si estado alarma es igual a 1 
      digitalWrite(sensorPin, HIGH); //el sensor comienza a funcionar
      Serial.println("prendido");
      if(sensorPin == 1){ //si el sensor detecta algo
        digitalWrite(alarmaPin, HIGH);} //el pin de la alarma libera un 1 que la hace sonar por 10 seg
        delay(10000);
      else{
        digitalWrite(alarmaPin, LOW);}//si el sensor no detecta nada, la alarma no se activa
      }
    else { //si estado alarma es 0 (o sea el boton de la web esta apagado)
      digitalWrite(sensorPin, LOW); //el sensor se apaga
      Serial.println("apagado"); //y la consola printea apagado
    }
  }
  else{
      Serial.print("error");
      Serial.println(firebaseData.errorReason()); //esto es lo anteriormente comentado acerca de si firebase falla
    } 
}

